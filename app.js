const express = require('express')
const bodyParser = require("body-parser");
var swaggerJsdoc = require('swagger-jsdoc');
var swaggerUi = require("swagger-ui-express");
var config = require('./middleware/config');
const app = express()

var swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: 'STUNTS DOC',
    version: '4.0.0',
    description: 'Startag Amadeus API',
    contact: {
      name: "Ardi Suryana",
      url: "https://swagger.io",
      email: "ardisuryana115@gmail.com"
    }
  },
  servers: [
    {
      url: "http://" + config.hostlocal + ":3050/"
    }
  ],
  host: "http://" + config.hostlocal + ":3050/",
  basePath: '/',
  components: {
  securitySchemes: {
    APIKey: {
      type: 'apiKey',
      in: 'header',
      name: 'Authorization',
    },
    TokenKey: {
      type: 'apiKey',
      in: 'header',
      name: "UserToken"
    }
  },
}
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js'],
};

const swaggerSpec = swaggerJsdoc(options);

app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
// End swagger

app.use(bodyParser.json({
  verify: (req, res, buf) => {
    req.rawBody = buf
  }
}));
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(function(req, res, next){
  var data = "";
  req.on('data', function(chunk){ data += chunk})
  req.on('end', function(){
     req.rawBody = data;
     next();
  })
})

// Route
app.use('/', require('./routes/airports'));
app.use('/', require('./routes/flight'));
app.use('/', require('./routes/hotel'));
app.use('/', require('./routes/code'));
app.use('/', require('./routes/other'));


app.get('*',function(req,res){
    res.redirect('http://startag.id/');
});

app.listen(3050)