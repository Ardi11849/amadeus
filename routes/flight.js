const express = require('express')
const app = express.Router();
var response = require('../config/response');
const auth = require('../middleware/auth');
const flightService = require('../seeders/flightService');


/**
* @swagger
* /flight/api/v1/all:
*   get:
*     tags:
*       - Filght
*     name: all service
*     summary: Running all service showFlight->pricing->booking
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/all', async function(req, res){
    await flightService.all(req.query.currency, req.query.origin, req.query.destination, req.query.page, req.query.limit, req.query.shorting, req.query.trip_class, req.query.token)
    .then(result => {
        response.ok(result.data, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/show:
*   get:
*     tags:
*       - Filght
*     name: Show Filghts
*     summary: List of user Filghts
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/show', async function(req, res){
    await flightService.getFlight(req.query.currency, req.query.origin, req.query.destination, req.query.page, req.query.limit, req.query.shorting, req.query.trip_class, req.query.token)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/price:
*   post:
*     tags:
*       - Filght
*     name: price Filghts
*     summary: List price 1 flight
*     requestBody:        
*       required: true
*       content:
*           text/plain:
*               schema:
*                   type: string
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.post('/flight/api/v1/price', async function(req, res){
    await flightService.pricing(req.rawBody)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/book:
*   post:
*     tags:
*       - Filght
*     name: booking Filghts
*     summary: Booking flight
*     requestBody:        
*       required: true
*       content:
*           text/plain:
*               schema:
*                   type: string
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.post('/flight/api/v1/book', async function(req, res){
    await flightService.booking(req.rawBody)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/cheapestDates:
*   get:
*     tags:
*       - Filght
*     name: Flight Cheapest Date Search
*     summary: See the list cheapest dates
*     parameters:
*       - in: query
*         name: origin
*         type: string
*         required: true
*       - in: query
*         name: destination
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/cheapestDates', async function(req, res){
    await flightService.cheapestDates(req.query.origin, req.query.destination)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/parserJob:
*   get:
*     tags:
*       - Filght
*     name: Parser Job
*     summary: To check status of the job with ID
*     parameters:
*       - in: query
*         name: id
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/parserJob', async function(req, res){
    await flightService.parserjobGet(req.query.id)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/parserJob/post:
*   get:
*     tags:
*       - Filght
*     name: Parser Job
*     summary: To check status of the job with ID
*     parameters:
*       - in: query
*         name: id
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/parserJob/post', async function(req, res){
    await flightService.parserjobPost(req.query.id)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/flight/busiestFlight:
*   get:
*     tags:
*       - Filght
*     name: Analytic flight busiest traveling period 
*     summary: Analytic flight busiest period
*     parameters:
*       - in: query
*         name: cityCode
*         type: string
*         required: true
*       - in: query
*         name: period
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/flight/busiestFlight', async function(req, res){
    await flightService.busiestMonth(req.query.cityCode, req.query.period)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/flight/delayPred:
*   get:
*     tags:
*       - Filght
*     name: Analytic flight delay traveling period 
*     summary: Analytic flight delay period
*     parameters:
*       - in: query
*         name: originLocationCode
*         type: string
*         required: true
*       - in: query
*         name: destinationLocationCode
*         type: string
*         required: true
*       - in: query
*         name: departureDate
*         type: string
*         required: true
*       - in: query
*         name: departureTime
*         type: string
*         required: true
*       - in: query
*         name: arrivalDate
*         type: string
*         required: true
*       - in: query
*         name: arrivalDate
*         type: string
*         required: true
*       - in: query
*         name: arrivalTime
*         type: string
*         required: true
*       - in: query
*         name: aircraftCode
*         type: string
*         required: true
*       - in: query
*         name: carrierCode
*         type: string
*         required: true
*       - in: query
*         name: flightNumber
*         type: string
*         required: true
*       - in: query
*         name: duration
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/flight/delayPred', async function(req, res){
    const query = req.query
    await flightService.flightDelayPrediction(query.originLocationCode, query.destinationLocationCode, query.departureDate, 
        query.departureTime, query.arrivalDate, query.arrivalTime, query.aircraftCode, query.carrierCode, 
        query.flightNumber, query.duration)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});


/**
* @swagger
* /flight/api/v1/flight/mostBookDest:
*   get:
*     tags:
*       - Filght
*     name:  Flight most booked destination
*     summary: Flight most booked destination analytic 
*     parameters:
*       - in: query
*         name: originCityCode
*         type: string
*         required: true
*       - in: query
*         name: period
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/flight/mostBookDest', async function(req, res){
    await flightService.mostBookedDes(req.query.originCityCode, req.query.period)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/flight/mostTravelDest:
*   get:
*     tags:
*       - Filght
*     name:  flight most traveled destinations
*     summary: Where people flying to from 'location' in 'period'?
*     parameters:
*       - in: query
*         name: originCityCode
*         type: string
*         required: true
*       - in: query
*         name: period
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/flight/mostTravelDest', async function(req, res){
    await flightService.mostTraveledDes(req.query.originCityCode, req.query.period)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /flight/api/v1/flight/seatMaps:
*   get:
*     tags:
*       - Filght
*     name:  seat maps
*     summary: Returns all the seat maps of a given order
*     parameters:
*       - in: query
*         name: flightOrderId
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/flight/api/v1/flight/seatMaps', async function(req, res){
    await flightService.seatMaps(req.query.flightOrderId)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});




module.exports = app;
