const express = require('express')
const app = express.Router();
var response = require('../config/response');
const auth = require('../middleware/auth');
const airportsService = require('../seeders/airportsService');

/**
* @swagger
* /airports/api/v1/airports/airportLatLong:
*   get:
*     tags:
*       - Airports
*     name: Airports around
*     summary: Airports around a specific location with longitude and latitude
*     parameters:
*       - in: query
*         name: longitude
*         type: string
*         required: true
*       - in: query
*         name: latitude
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/airports/api/v1/airports/airportLatLong', async function(req, res){
    await airportsService.airPortLatLon(req.query.longitude, req.query.latitude)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /airports/api/v1/airports/onTimePrediction:
*   get:
*     tags:
*       - Airports
*     name: Airports Prediction on time
*     summary: Airports Prediction on time
*     parameters:
*       - in: query
*         name: airportCode
*         type: string
*         required: true
*       - in: query
*         name: date
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/airports/api/v1/airports/onTimePrediction', async function(req, res){
    await airportsService.airPortPredictionOntime(req.query.airportCode, req.query.date)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

module.exports = app;