const express = require('express')
const app = express.Router();
var response = require('../config/response');
const auth = require('../middleware/auth');
const hotelService = require('../seeders/hotelService');

/**
* @swagger
* /hotel/api/v1/show:
*   get:
*     tags:
*       - Hotel
*     name: Show Hotels
*     summary: List of user Hotels
*     parameters:
*       - in: query
*         name: city
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/hotel/api/v1/show', async function(req, res){
    await hotelService.getHotel(req.query.city)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /hotel/api/v1/show/detail:
*   get:
*     tags:
*       - Hotel
*     name: Show Hotels
*     summary: List of detail Hotels
*     parameters:
*       - in: query
*         name: id
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/hotel/api/v1/show/detail', async function(req, res){
    await hotelService.getHotelDetail(req.query.id)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /hotel/api/v1/show/detail/room:
*   get:
*     tags:
*       - Hotel
*     name: Show Hotels
*     summary: List of detail Hotels
*     parameters:
*       - in: query
*         name: id
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/hotel/api/v1/show/detail/room', async function(req, res){
    await hotelService.getHotelDetailRoom(req.query.id)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /hotel/api/v1/book:
*   post:
*     tags:
*       - Hotel
*     name: booking Hotels
*     summary: Booking hotel
*     requestBody:        
*       required: false
*       content:
*           text/plain:
*               schema:
*                   type: string
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.post('/hotel/api/v1/book', async function(req, res){
    await hotelService.booking(req.rawBody)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /hotel/api/v1/reputation:
*   get:
*     tags:
*       - Hotel
*     name:  Hotel Ratings
*     summary: List of Hotel Ratings
*     parameters:
*       - in: query
*         hotelsId: id hotel
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/hotel/api/v1/reputation/', async function(req, res){
    await hotelService.getHotelRatings(req.query.hotelsId)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

module.exports = app;
