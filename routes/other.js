const express = require('express')
const app = express.Router();
var response = require('../config/response');
const auth = require('../middleware/auth');
const otherService = require('../seeders/otherService');

/**
* @swagger
* /other/api/v1/other/poi:
*   get:
*     tags:
*       - Other
*     name: POI
*     summary: popular places (based on a geo location and a radius)
*     parameters:
*       - in: query
*         name: latitude
*         type: string
*         required: true
*       - in: query
*         name: longitude
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/other/api/v1/other/poi', async function(req, res){
    await otherService.poi(req.query.latitude, req.query.longitude)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /other/api/v1/other/safePlace:
*   get:
*     tags:
*       - Other
*     name: Safe Place
*     summary: Safe place (based on a geo location and a radius)
*     parameters:
*       - in: query
*         name: latitude
*         type: string
*         required: true
*       - in: query
*         name: longitude
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Return picture
*/
app.get('/other/api/v1/other/safePlace', async function(req, res){
    await otherService.safePlace(req.query.latitude, req.query.longitude)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

module.exports = app;