const express = require('express')
const app = express.Router();
var response = require('../config/response');
const auth = require('../middleware/auth');
const codeService = require('../seeders/codeService');

/**
* @swagger
* /api/v1/city_code:
*   get:
*     tags:
*       - Code
*     name: City Code
*     summary: List code of city
*     parameters:
*       - in: query
*         name: city
*         type: string
*         required: false
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Show cities code
*/
app.get('/api/v1/city_code', async function(req, res){
    await codeService.cities(req.query.city)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /api/v1/airport:
*   get:
*     tags:
*       - Code
*     name: airport Code
*     summary: List code of airport
*     parameters:
*       - in: query
*         name: airport
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Show airport code
*/
app.get('/api/v1/airport', async function(req, res){
    await codeService.airports(req.query.airport)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

/**
* @swagger
* /api/v1/airline:
*   get:
*     tags:
*       - Code
*     name: airline Code
*     summary: List code of airline
*     parameters:
*       - in: query
*         name: airline
*         type: string
*         required: true
*     security:
*       - APIKey: []
*       - TokenKey: []
*     responses:
*       200:
*         description: Show airline code
*/
app.get('/api/v1/airline', async function(req, res){
    await codeService.airlines(req.query.airline)
    .then(result => {
        response.ok(result, res);
        console.log(result);
    }).catch(err => {
        response.error(err, res);
    })
});

module.exports = app;
