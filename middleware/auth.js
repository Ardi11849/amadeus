var Auth = {
    common: require("./common"),
    service: require("./services"),
    config: require("./config"),

    activeUsername: null,

    authorization: function(req, res, next) {
        Auth.common.log(req.headers["authorization"]);
        if (Auth.common.isNullorEmpty(req.headers["authorization"]))
            return res.status(499).send("Unauthorized request token.");
        else {
            try {
                if (Auth.common.decryptor(req.headers["authorization"]) != Auth.config.appId)
                    return res.status(401).send("Invalid authentication");
            } catch (err) {
                res.status(401).send("Invalid authentication");
            }
        }
        
        next();
    },

    userAuthentication: function (req, res, next) {
        Auth.common.log(req.headers["authorization"]);
        Auth.common.log(req.headers["usertoken"]);
        Auth.common.log(JSON.stringify(req.headers));

        if (Auth.common.isNullorEmpty(req.headers["authorization"]))
            return res.status(499).send("Unauthorized request token.");
        else if (Auth.common.isNullorEmpty(req.headers["usertoken"]))
            return res.status(499).send("Unauthorized user token.");
        else {
            try {
                if (Auth.common.decryptor(req.headers["authorization"]) != Auth.config.appId)
                    return res.status(401).send("Invalid authentication");
                else Auth.activeUsername = Auth.common.decryptorUsername(req.headers["usertoken"]);
            } catch (err) {
                res.status(401).send("Invalid authentication");
            }
        }
        
        next();
    }
}

module.exports = Auth;