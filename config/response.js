exports.ok = function(values, res) {
    var data = {
        data: values,
        lastRequested: Math.floor(Date.now()/1000)
    };

    res.json(data);
    res.end();
};

exports.pagination = function(values, totalPages, res) {
    var data = {
        data: values,
        totalPages: totalPages,
        lastRequested: Math.floor(Date.now()/1000)
    };

    res.json(data);
    res.end();
};

exports.message = function(msg, res) {
    res.json({message: msg});
    res.end();
};

exports.error = function (error, res) {
    console.log(JSON.stringify(error));
    res.status(500).send(error.message);
};