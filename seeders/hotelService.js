const { ResponseError } = require('amadeus/lib/amadeus/client/errors');
const { json, response } = require('express');

var hotelService = {
  db: require('../config/db'),
  common: require('../middleware/common'),
  config: require('../middleware/config'),
  error: require('../middleware/error'),

  setting: ()=>{
    var Amadeus = require('amadeus');
    var amadeus = new Amadeus({
      clientId: '8JjAe4Gr1ZbWKHa37DarxY2VvqHhUuIZ',
      clientSecret: 'Z3PvOEIoya152pt8'
    });
    return amadeus;
  },


  handleErrors: (response) => {
    if(!response.ok) {
      throw new Error("Request failed " + response.statusText);
    }
    return response;
  },

  getHotel: async(city) => {
    var amadeus = hotelService.setting();
    try {
      var isi = amadeus.shopping.hotelOffers.get({
        // cityCode : 'MAD'
        cityCode: city
      }).then((response)=>{
        return response.data;
      }).catch((responseError)=>{
        return responseError;
      })
      return isi;
    } catch (error) {
      throw error;
    }
  },

  getHotelDetail: async(id) => {
    var amadeus = hotelService.setting();
    try {
      var isi = amadeus.shopping.hotelOffersByHotel.get({
        hotelId: id
      }).then((response)=>{
        return response.data;
      }).catch((responseError)=>{
        return responseError;
      })
      return isi;
    } catch (error) {
      throw error;
    }
  },

  getHotelDetailRoom: async(id) => {
    var amadeus = hotelService.setting();
    try {
      var isi = amadeus.shopping.hotelOffer(id).get().then((response)=>{
        return response.data;
      }).catch((responseError)=>{
        return responseError;
      })
      return isi;
    } catch (error) {
      throw error;
    }
  },

  getHotelRatings: async(hotelsId) => {
    var amadeus = hotelService.setting();
    try {
      var isi = amadeus.eReputation.hotelSentiments.get({
        hotelIds:hotelsId
      }).then((response)=>{
        console.log(response)
        return response.data;
      }).catch((responseError)=>{
        return responseError;
      })
      return isi;
    } catch (error) {
      throw error;
    }
  },

  pricing: async(dataPenerbangan)=>{
    var amadeus = hotelService.setting();
    var data = dataPenerbangan.replace(/\s/g, '');
    
    try {
      HotelOffers = amadeus.shopping.HotelOffers.pricing.post(
       JSON.stringify({"data":{"type":"Hotel-offers-pricing","HotelOffers":[JSON.parse(data)]}})
      ).then(function(price){
        return price.data;
      }).catch(function(responseError){
        console.log(responseError);
        return responseError;
      });
      return HotelOffers;
    } catch (error) {
      throw error;
    }
  },

  booking: async(data)=>{
    console.log(JSON.parse(data)[0]);
    var data = data.replace(/\s/g, '');
    
    const amadeus = hotelService.setting();
    try{
      var booking = amadeus.booking.hotelBookings.post(
        JSON.stringify(
          JSON.parse(data)
        )
      ).then(function(response){
          return response.data;
      }).catch(function(responseError){
        console.log(responseError);
        return responseError;
      });
      return booking;
    }catch(error){
      throw error;
    }
  },

  all: async()=>{
    const amadeus = hotelService.setting();
    try{
      const returnBokkin = amadeus.shopping.HotelOffersSearch.get({
        originLocationCode: 'SYD',
        destinationLocationCode: 'BKK',
        departureDate: '2020-08-01',
        adults: '1'
      }).then(function(HotelOffers){
        // return HotelOffers.data[1];
        HotelOffers = amadeus.shopping.HotelOffers.pricing.post(
          JSON.stringify({
            'data': {
              'type': 'Hotel-offers-pricing',
              'HotelOffers': [HotelOffers.data[1]]
            }
          })
        ).then(function(price){
          // console.log(price.data.HotelOffers);
          
          // return price;
            var booking = amadeus.booking.HotelOrders.post(
              JSON.stringify({
                "data": {
                  "type": "Hotel-order",
                  "HotelOffers": price.data.HotelOffers,
                  "travelers": [
                    {
                      "id": "1",
                      "dateOfBirth": "1982-01-16",
                      "name": {
                        "firstName": "JORGE",
                        "lastName": "GONZALES"
                      },
                      "gender": "MALE",
                      "contact": {
                        "emailAddress": "jorge.gonzales833@telefonica.es",
                        "phones": [
                          {
                            "deviceType": "MOBILE",
                            "countryCallingCode": "34",
                            "number": "480080076"
                          }
                        ]
                      },
                      "documents": [
                        {
                          "documentType": "PASSPORT",
                          "birthPlace": "Madrid",
                          "issuanceLocation": "Madrid",
                          "issuanceDate": "2015-04-14",
                          "number": "00000000",
                          "expiryDate": "2025-04-14",
                          "issuanceCountry": "ES",
                          "validityCountry": "ES",
                          "nationality": "ES",
                          "holder": true
                        }
                      ]
                    }
                  ]
                }
              })
            )
              console.log(booking);
              return booking;
        }).catch(function(priceError){
            console.log(priceError);
            return priceError;
        });
        return HotelOffers;
      }).catch(function(HotelOffersError){
        console.log(HotelOffersError);
        return HotelOffersError;
      });
      return returnBokkin;
    }catch(error){
      throw error;
    }
  }
}
module.exports = hotelService;
