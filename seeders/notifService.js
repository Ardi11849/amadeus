var notifikasiService = {
  admin : require("firebase-admin"),
  serviceAccount : require("../middleware/xmod-29026-firebase-adminsdk-elemn-c49710967c.json"),
  db: require('../config/db'),
  common: require('../middleware/common'),
  config: require('../middleware/config'),
  error: require('../middleware/error'),

  addFriend : async(from, to) => {
    const conn = await notifikasiService.db.connection.getConnection();
    console.log(from);    
    try {
        var sql = "SELECT device_id, username FROM accounts WHERE rowstatus = 1 AND username = ?";
        var account = await notifikasiService.db.single(sql, [to]);

        if (account.length == 0) throw new notifikasiService.error.BadRequest("Target notification not found.");

        var devicedb = account[0]["device_id"];
        var usernamedb = account[0]["username"];
        var title = "Pertemanan";
        var body = "Menambahkan Pertemanan Baru"
        console.log(devicedb);
        var message = {
          notification: {
            title: "Pertemanan Baru dari " + from,
            body: "Hi"+usernamedb+', '+from+" meminta pertemanan baru dengan anda",
            // imageUrl: 'https://w7.pngwing.com/pngs/791/922/png-transparent-react-javascript-library-redux-user-interface-tesseract-logo-symmetry-nodejs.png'
          },
          // data: {
          //   screen: "850",
          //   username: usernamedb,
          // },
          token: devicedb,
        };

        // Send a message to the device corresponding to the provided
        // registration token.
        notifikasiService.admin
          .messaging()
          .send(message)
          .then( async (response) => {
            // Response is a message ID string.
            console.log("Successfully sent message:", response);
            var userData = [
              from,
              to,
              title,
              body
          ];

          var sql = "INSERT INTO notifications (" + 
                      "`from`, " +
                      "`to`, " +
                      "`type`, " +
                      "`title`, " +
                      "`body`, " +
                      "`status`, " +
                      "`createdon`) " +
                      "VALUES (?,?,1,?,?,0,now())";
          
          await conn.beginTransaction();
          var result = await conn.query(sql, userData);
          await conn.commit();
          conn.release();
          console.log(userData);
          return response;
          })
          .catch((error) => {
            console.log("Error sending message:", error);
            throw error;
          });
    }catch (err) {
        await conn.query("ROLLBACK");
        conn.release();
        throw err;
    }
  },

  totalNotifications : async(to) => {
    console.log(to);
    
    try{
      var sql = "SELECT `to` FROM notifications WHERE status = 0 AND `to` = ?";
      var notifikasi = await notifikasiService.db.single(sql, [to]);

      if (notifikasi.length == 0) throw new notifikasiService.error.BadRequest("User Tidak Meliliki Notifikasi");
      return notifikasi.length;
    }catch(error){
      throw error;
    }
  },

  notifications: async(username, page) =>{
      try {
        page = page - 1;
        var offset = 50;
        var sql = "SELECT "+
                  "`from`, "+
                  "`title`, "+
                  "`body`,  "+
                  "`status`,  "+
                  "TIMESTAMPDIFF(MINUTE, createdon, NOW())"+
                  "FROM notifications  "+
                  "WHERE `to` = ?"+
                  "ORDER BY createdon DESC LIMIT ?, ?";
        page = page * offset;
        var notifikasi = await notifikasiService.db.all(sql, [username, page, offset]);
        console.log(username);
        console.log(page);
        console.log(notifikasi)
        return notifikasi[0];
      } catch (err) {
          throw err;
      }
  },

  updateStatus: async(to) => {
    const conn = await notifikasiService.db.connection.getConnection();
    try{
      await conn.beginTransaction();
      var sql = "UPDATE notifications SET status = 1, modifiedon = now() WHERE `to` = ? AND status = 0";
      var result = await notifikasiService.db.update(sql, [to]);
    }catch(error){
      throw error;
    }
  }
}
module.exports = notifikasiService;
