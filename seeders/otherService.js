var otherService = {
    db: require('../config/db'),
    common: require('../middleware/common'),
    config: require('../middleware/config'),
    error: require('../middleware/error'),
  
    setting: ()=>{
      var Amadeus = require('amadeus');
      var amadeus = new Amadeus({
        clientId: '8JjAe4Gr1ZbWKHa37DarxY2VvqHhUuIZ',
        clientSecret: 'Z3PvOEIoya152pt8'
      });
      return amadeus;
    },

    handleErrors: (response) => {
        if(!response.ok) {
          throw new Error("Request failed " + response.statusText);
        }
        return response;
    },

    poi: async(latitude, longitude)=>{  
        const amadeus = otherService.setting();
        try{
          var data = amadeus.referenceData.locations.pointsOfInterest.get({
            latitude: latitude,
            longitude: longitude
          }).then(function(response){
            return response.data;
          }).catch(function(responseError){
            return responseError;
          });
          return data;
        }catch(error){
          throw error;
        }
      },

      safePlace: async(latitude, longitude)=>{  
        const amadeus = otherService.setting();
        try{
          var data = amadeus.safety.safetyRatedLocations.get({
            latitude: latitude,
            longitude: longitude
          }).then(function(response){
            return response.data;
          }).catch(function(responseError){
            return responseError;
          });
          return data;
        }catch(error){
          throw error;
        }
      },

}

module.exports = otherService;