var airportsService = {
    db: require('../config/db'),
    common: require('../middleware/common'),
    config: require('../middleware/config'),
    error: require('../middleware/error'),
  
    setting: ()=>{
      var Amadeus = require('amadeus');
      var amadeus = new Amadeus({
        clientId: '8JjAe4Gr1ZbWKHa37DarxY2VvqHhUuIZ',
        clientSecret: 'Z3PvOEIoya152pt8'
      });
      return amadeus;
    },

    handleErrors: (response) => {
        if(!response.ok) {
          throw new Error("Request failed " + response.statusText);
        }
        return response;
    },

    airPortLatLon: async(longitude, latitude)=>{  
        const amadeus = airportsService.setting();
        try{
          var data = amadeus.referenceData.locations.airports.get({
            longitude: longitude,
            latitude: latitude
          }).then(function(response){
            return response.data;
          }).catch(function(responseError){
            return responseError;
          });
          return data;
        }catch(error){
          throw error;
        }
      },

      airPortPredictionOntime: async(airportCode, date)=>{  
        const amadeus = airportsService.setting();
        try{
          var data = amadeus.airport.predictions.onTime.get({
            airportCode: airportCode,
            date: date
          }).then(function(response){
            return response.data;
          }).catch(function(responseError){
            return responseError;
          });
          return data;
        }catch(error){
          throw error;
        }
      },  
      

}
module.exports = airportsService;