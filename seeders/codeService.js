var codeService = {
    db: require('../config/db'),
    common: require('../middleware/common'),
    config: require('../middleware/config'),
    error: require('../middleware/error'),
  
  
    handleErrors: (response) => {
      if(!response.ok) {
        throw new Error("Request failed " + response.statusText);
      }
      return response;
    },
  
    cities: async(city) => {
      // const travelpayouts = require('travelpayouts-api');
      // const api = new travelpayouts('7abf1ea61f6ead2bfad6d37be1d96203', '286539');
      // var http = require('http');
      const fetch = require("node-fetch");
      const url = "http://api.travelpayouts.com/data/en/cities.json";
      console.log(url);
      
      try {
        const isi = fetch(url)
        .then(codeService.handleErrors)
        .then(res => res.json())
        .then(function(data) {
          if(!city){
            return data
          }else{
            const ambil = data.find(item => item.name === city || item.name.toString().toLowerCase() === city)
            return ambil;
          }
        })
        .catch(function(err) {
            console.log(err);
        });
        console.log(isi);
        return isi;
      } catch (error) {
        throw error;
      }
    },

    airports: async(airport) => {
        // const travelpayouts = require('travelpayouts-api');
        // const api = new travelpayouts('7abf1ea61f6ead2bfad6d37be1d96203', '286539');
        // var http = require('http');
        const fetch = require("node-fetch");
        const url = "http://api.travelpayouts.com/data/en/airports.json";
        console.log(url);
        
        try {
          const isi = fetch(url)
          .then(codeService.handleErrors)
          .then(res => res.json())
          .then(function(data) {
              const ambil = data.find(item => item.name === airport)
              console.log(ambil) ;  //expecting array
              return ambil;
          })
          .catch(function(err) {
              console.log(err);
          });
          console.log(isi);
          return isi;
        } catch (error) {
          throw error;
        }
      },

    airlines: async(airline) => {
        // const travelpayouts = require('travelpayouts-api');
        // const api = new travelpayouts('7abf1ea61f6ead2bfad6d37be1d96203', '286539');
        // var http = require('http');
        const fetch = require("node-fetch");
        const url = "http://api.travelpayouts.com/data/en/airlines.json";
        console.log(url);
        
        try {
          const isi = fetch(url)
          .then(codeService.handleErrors)
          .then(res => res.json())
          .then(function(data) {
              const ambil = data.find(item => item.name === airline)
              console.log(ambil) ;  //expecting array
              return ambil;
          })
          .catch(function(err) {
              console.log(err);
          });
          console.log(isi);
          return isi;
        } catch (error) {
          throw error;
        }
      }
  }
  module.exports = codeService;
  